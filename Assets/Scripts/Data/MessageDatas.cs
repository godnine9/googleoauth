﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YT.Message;

[CreateAssetMenu(fileName = "MessageDatas", menuName = "CreateData/Create MessageDatas", order = 1)]
[System.Serializable]
public class MessageDatas : ScriptableObject
{
    public LiveMessage[] LiveMessages;
    public LiveMessage[] TestMessages;
}

[System.Serializable]
public class LiveMessage
{
    public string WhoSayWhat;
    public string Name;
    public bool IsChatOwner;
    public string authorChannelId;
    public string publishedAt;
    public string displayMessage;

    public LiveMessage(string iName,bool iIsChatOwner, string iauthorChannelId, string ipublishedAt, string idisplayMessage)
    {
        WhoSayWhat = iName + ":" + idisplayMessage;
        Name = iName;
        IsChatOwner = iIsChatOwner;
        authorChannelId = iauthorChannelId;
        publishedAt = ipublishedAt;
        displayMessage = idisplayMessage;
    }
}

