﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YT.Subscribers;

[CreateAssetMenu(fileName = "SubscribersData", menuName = "CreateData/Create SubscribersData", order = 1)]
[System.Serializable]
public class SubscribersData : ScriptableObject
{
    public Subscriber[] Subscribers;
}

[System.Serializable]
public class Subscriber
{
    public string Name;
    public string Description;
    public string ChannelID;
    public string IconURL;

    public Subscriber(string iName, string iDescription, string iChannelID, string iIconURL)
    {
        Name = iName;
        Description = iDescription;
        ChannelID = iChannelID;
        IconURL = iIconURL;
    }
}