using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OAuthSetting", menuName = "CreateData/Create OAuthSetting", order = 1)]
[System.Serializable]
public class OAuthSetting : ScriptableObject
{
    public string _clientId;
    public string _clientSecret;
}
