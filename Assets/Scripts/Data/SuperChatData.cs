﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YT.SuperChat;

[CreateAssetMenu(fileName = "SuperChatData", menuName = "CreateData/Create SuperChatData", order = 1)]
[System.Serializable]
public class SuperChatData : ScriptableObject
{
    public SuperChat[] SuperChats;
}

[System.Serializable]
public class SuperChat
{
    public string ID;
    public string displayName;
    public string profileImageUrl;
    public long publishTime;
    public Snippet Snippet;
    public bool IsPreformed;

    public SuperChat (string iID, string idisplayName, string iprofileImageUrl, long ipublishTime, Snippet iSnippet)
    {
        ID = iID;
        displayName = idisplayName;
        profileImageUrl = iprofileImageUrl;
        publishTime = ipublishTime;
        Snippet = iSnippet;
        IsPreformed = false;
    }
}