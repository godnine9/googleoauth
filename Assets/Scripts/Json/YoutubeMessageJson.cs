﻿namespace YT.Message
{
    [System.Serializable]
    public class PageInfo
    {
        public int totalResults;
        public int resultsPerPage;
    }
    [System.Serializable]
    public class TextMessageDetails
    {
        public string messageText;
    }

    [System.Serializable]
    public class Snippet
    {
        public string type;
        public string liveChatId;
        public string authorChannelId;
        public string publishedAt;
        public bool hasDisplayContent;
        public string displayMessage;
        public TextMessageDetails textMessageDetails;
    }
    [System.Serializable]
    public class AuthorDetails
    {
        public string channelId;
        public string channelUrl;
        public string displayName;
        public string profileImageUrl;
        public bool isVerified;
        public bool isChatOwner;
        public bool isChatSponsor;
        public bool isChatModerator;
    }
    [System.Serializable]
    public class Item
    {
        public string kind;
        public string etag;
        public string id;
        public Snippet snippet;
        public AuthorDetails authorDetails;
    }

    [System.Serializable]
    public class Root
    {
        public string kind;
        public string etag;
        public int pollingIntervalMillis;
        public PageInfo pageInfo;
        public string nextPageToken;
        public Item[] items;
    }
}