﻿namespace YT.Subscribers
{
    [System.Serializable]
    public class PageInfo
    {
        public int totalResults;
        public int resultsPerPage;
    }
    [System.Serializable]
    public class Default
    {
        public string url;
    }
    [System.Serializable]
    public class Medium
    {
        public string url;
    }
    [System.Serializable]
    public class High
    {
        public string url;
    }
    [System.Serializable]
    public class Thumbnails
    {
        public Default @default;
        public Medium medium;
        public High high;
    }
    [System.Serializable]
    public class SubscriberSnippet
    {
        public string title;
        public string description;
        public string channelId;
        public Thumbnails thumbnails;
    }
    [System.Serializable]
    public class Item
    {
        public string kind;
        public string etag;
        public string id;
        public SubscriberSnippet subscriberSnippet;
    }
    [System.Serializable]
    public class Root
    {
        public string kind;
        public string etag;
        public string nextPageToken;
        public string prevPageToken;
        public PageInfo pageInfo;
        public Item[] items;
    }
}