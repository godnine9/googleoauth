﻿namespace YT.ChatRoom
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    [System.Serializable]
    public class PageInfo
    {
        public int totalResults;
        public int resultsPerPage;
    }
    [System.Serializable]
    public class Default
    {
        public string url;
        public int width;
        public int height;
    }
    [System.Serializable]
    public class Medium
    {
        public string url;
        public int width;
        public int height;
    }
    [System.Serializable]
    public class High
    {
        public string url;
        public int width;
        public int height;
    }
    [System.Serializable]
    public class Standard
    {
        public string url;
        public int width;
        public int height;
    }
    [System.Serializable]
    public class Maxres
    {
        public string url;
        public int width;
        public int height;
    }
    [System.Serializable]
    public class Thumbnails
    {
        public Default @default;
        public Medium medium;
        public High high;
        public Standard standard;
        public Maxres maxres;
    }
    [System.Serializable]
    public class Snippet
    {
        public string publishedAt;
        public string channelId;
        public string title;
        public string description;
        public Thumbnails thumbnails;
        public string scheduledStartTime;
        public string actualStartTime;
        public bool isDefaultBroadcast;
        public string liveChatId;
    }
    [System.Serializable]
    public class Item
    {
        public string kind;
        public string etag;
        public string id;
        public Snippet snippet;
    }
    [System.Serializable]
    public class Root
    {
        public string kind;
        public string etag;
        public PageInfo pageInfo;
        public Item[] items;
    }

}