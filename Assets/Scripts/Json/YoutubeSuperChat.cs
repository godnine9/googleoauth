namespace YT.SuperChat
{
    [System.Serializable]
    public class PageInfo
    {
        public int totalResults;
        public int resultsPerPage;
    }
    [System.Serializable]
    public class SupporterDetails
    {
        public string channelId;
        public string channelUrl;
        public string displayName;
        public string profileImageUrl;
    }
    [System.Serializable]
    public class Snippet
    {
        public string channelId;
        public SupporterDetails supporterDetails;
        public string commentText;
        public string createdAt;
        public string amountMicros;
        public string currency;
        public string displayString;
        public int messageType;
    }
    [System.Serializable]
    public class Item
    {
        public string kind;
        public string etag;
        public string id;
        public Snippet snippet;
    }
    [System.Serializable]
    public class Root
    {
        public string kind;
        public string etag;
        public string nextPageToken;
        public PageInfo pageInfo;
        public Item[] items;
    }
}