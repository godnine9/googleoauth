﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class Wheel : MonoBehaviour
{
    public RectTransform WheelRoot;
    public TMP_Text TotalNum;
    public RectTransform Arrow;
    public RandonData[] Datas;
    public string DefulaString;
    public int WheelRadius = 250;
    public List<Image> Images;
    public TMP_FontAsset CustomFont;
    public float WheelTextRadius = 0.8f;
    public bool WheelPreform;
    public ParticleSystem Effect;
    private RectTransform WheelBaseRect;
    private List<float> AllNum = new List<float>();
    private GamePlayManager mGamePlayManager;
    public void Init(GamePlayManager iGamePlayManager)
    {
        mGamePlayManager = iGamePlayManager;
        Arrow.anchoredPosition = new Vector2(0, WheelRadius);
        CreateWheel();
        Arrow.transform.SetAsLastSibling();
        TotalNum.transform.SetAsLastSibling();
        WheelRoot.localScale = Vector3.zero;
        TotalNum.text = DefulaString;
    }

    public void Show()
    {
        if (!WheelPreform)
        {
            ShowWheel();
        }
    }

    public void Hide()
    {
        StopAllCoroutines();
        WheelRoot.DOScale(Vector3.zero, 0.1f);
        WheelPreform = false;
    }

    public void RotateWheel(bool iFake = false)
    {
        if (WheelPreform) { return; }
        if(iFake)
        {
            StartCoroutine(FakeRotateWheelPreform());
        }
        else
        {
            StartCoroutine(RotateWheelPreform());
        }
    }

    private void ShowWheel()
    {
        TotalNum.text = DefulaString;
        TotalNum.gameObject.SetActive(true);
        TotalNum.rectTransform.localScale = Vector3.one;
        TotalNum.rectTransform.anchoredPosition = Vector2.zero;
        WheelRoot.DOScale(new Vector3(1, 1, 1), 0.1f);
        WheelBaseRect.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
    }

    private IEnumerator FakeRotateWheelPreform()
    {
        WheelPreform = true;
        int NumIndex = Random.Range(0, AllNum.Count);
        int DataIndex = -1;
        for (int i = 0; i < Datas.Length; i++)
        {
            if (Datas[i].Num == AllNum[NumIndex])
            {
                DataIndex = i;
            }
        }
        RandonData aTargetData = Datas[DataIndex];
        float UnitAngle = 360.0f / (float)AllNum.Count;
        float TragetAngle = UnitAngle * (float)NumIndex + 3602.5f;
        yield return new WaitForSeconds(0.5f);
        WheelBaseRect.DOLocalRotate(new Vector3(0, 0, TragetAngle), 5, RotateMode.LocalAxisAdd).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(5.5f);
        aTargetData.OnResult.Invoke();
        yield return new WaitForSeconds(3.0f);
        Hide();
    }

    private IEnumerator RotateWheelPreform()
    {
        WheelPreform = true;
        ShowWheel();
        int NumIndex = Random.Range(0, AllNum.Count);
        int DataIndex = -1;
        for(int i =0;i<Datas.Length;i++)
        {
            if(Datas[i].Num == AllNum[NumIndex])
            {
                DataIndex = i;
            }
        }
        RandonData aTargetData = Datas[DataIndex];
        float UnitAngle = 360.0f / (float)AllNum.Count;
        float TragetAngle = UnitAngle * (float)NumIndex + 3602.5f;
        yield return new WaitForSeconds(0.5f);
        WheelBaseRect.DOLocalRotate(new Vector3(0, 0, TragetAngle), 5, RotateMode.LocalAxisAdd).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(5.5f);
        aTargetData.OnResult.Invoke();
        yield return new WaitForSeconds(2.0f);
        aTargetData.AfterResult.Invoke();
        yield return new WaitForSeconds(3.0f);
        Hide();
    }

    private void CreateWheel()
    {
        float AllNumCount = 0;
        float CurCount = 0;
        for (int i = 0; i < Datas.Length; i++)
        {
            AllNumCount += Datas[i].Count;
        }
        GameObject Base = new GameObject("WheelBase");
        Base.transform.SetParent(this.transform);
        Image aBaseImg = Base.AddComponent<Image>();
        aBaseImg.sprite = Resources.Load<Sprite>("Circle");
        aBaseImg.rectTransform.anchoredPosition = Vector2.zero;
        aBaseImg.rectTransform.sizeDelta = new Vector2(WheelRadius * 2 + 10, WheelRadius * 2 + 10);
        WheelBaseRect = aBaseImg.rectTransform;
        for (int i = 0; i < Datas.Length; i++)
        {
            GameObject aImgGo = new GameObject($"Base {Datas[i].DisplayName}");
            GameObject aNumGo = new GameObject($"Text {Datas[i].DisplayName}");
            aImgGo.transform.SetParent(Base.transform);
            aNumGo.transform.SetParent(Base.transform);
            Image aImg = aImgGo.AddComponent<Image>();
            TextMeshProUGUI aNum = aNumGo.AddComponent<TextMeshProUGUI>();
            float UnitAngle = -360 / AllNumCount;
            float Rate = Datas[i].Count / AllNumCount;
            float CircleAngle = UnitAngle * CurCount;
            float TextInCircleAngle = UnitAngle * (CurCount + (Datas[i].Count / 2));
            float TextAngle = UnitAngle * (Datas[i].Count / 2);
            aImg.type = Image.Type.Filled;
            aImg.fillAmount = Rate;
            aImg.color = Datas[i].Color;
            aImg.sprite = Resources.Load<Sprite>("Circle");
            aImg.rectTransform.sizeDelta = new Vector2(WheelRadius * 2, WheelRadius * 2);
            aImg.rectTransform.anchoredPosition = Vector2.zero;
            aImg.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, CircleAngle - 90));

            aNum.rectTransform.sizeDelta = new Vector2(200, 100);
            aNum.text = Datas[i].DisplayName;
            aNum.font = CustomFont;
            aNum.fontSize = 50;
            aNum.alignment = TextAlignmentOptions.Midline;
            aNum.rectTransform.anchoredPosition = new Vector2((-WheelRadius * WheelTextRadius) * Mathf.Cos(TextInCircleAngle * Mathf.Deg2Rad), (-WheelRadius * WheelTextRadius) * Mathf.Sin(TextInCircleAngle * Mathf.Deg2Rad));
            aNumGo.transform.SetParent(aImgGo.transform);
            aNum.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, TextAngle-90));
            CurCount += Datas[i].Count;
            Images.Add(aImg);

            for(int j=0;j< Datas[i].Count;j++)
            {
                AllNum.Add(Datas[i].Num);
            }
        }
    }

    public void AddTimePreform(int iValue)
    {
        TotalNum.rectTransform.DOShakePosition(0.5f, 30).OnComplete(() =>
        {
            Effect.Play();
            TotalNum.text = (8 + iValue).ToString();
        });
    }
    public void MultiplyTimePreform(string iValue)
    {
        TotalNum.rectTransform.DOShakePosition(0.5f, 30).OnComplete(() =>
        {
            Effect.Play();
            TotalNum.text = iValue;
        });
    }

    public void JumpToAddTime(int iValue)
    {
        TotalNum.rectTransform.DOJumpAnchorPos(new Vector2(-532, -245), 200, 1, 0.5f);
        TotalNum.rectTransform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f).OnComplete(() =>
        {
            TotalNum.gameObject.SetActive(false);
            mGamePlayManager.timmer.AddTime((8 + iValue) * 60);
        });
    }
    public void JumpToMultiplyTime(float iValue)
    {
        TotalNum.rectTransform.DOJumpAnchorPos(new Vector2(-532, -245), 200, 1, 0.5f);
        TotalNum.rectTransform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f).OnComplete(() =>
        {
            TotalNum.gameObject.SetActive(false);
            mGamePlayManager.timmer.MultiplyTime(iValue);
        });
    }

    public void Update()
    {
        if(!WheelPreform && Input.GetKeyDown(KeyCode.Mouse0) && WheelRoot.localScale.x == 1)
        {
            RotateWheel(true);
        }
    }
}

[System.Serializable]
public struct RandonData
{
    public string DisplayName;
    public float Num;
    public float Count;
    public Color32 Color;
    public UnityEvent OnResult;
    public UnityEvent AfterResult;
}