using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Strength : MonoBehaviour
{
    public TMP_InputField StrengthInput;
    public int strength;
    public void Init(int iStrength)
    {
        strength = iStrength;
        StrengthInput.text = strength.ToString();
        StrengthInput.onEndEdit.AddListener(EndEditTimer);
    }

    private void EndEditTimer(string iValue)
    {
        strength = int.Parse(iValue);
        strength = strength > 30 ? 30 : strength;
        StrengthInput.text = strength.ToString();
    }

    public void AddStrength(int iValue)
    {
        strength += iValue;
        strength = strength > 30 ? 30 : strength;
        StrengthInput.text = strength.ToString();
    }
}
