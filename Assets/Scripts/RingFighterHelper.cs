﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class RingFighterHelper : MonoBehaviour
{
    public enum GameStatus
    {
        Start,
        WaitToken,
        WaitLiveRoom,
        Gameing,
    }
    public GameStatus CurStatus;
    [Header("Start UI")]
    public GameObject StartUI;
    public Button StartButton;
    [Header("Token UI")]
    public GameObject GetAccessUI;
    public TMP_Text Title;
    public Text Content;
    public Button GetAccessButton;
    public Button NextStep;
    [Header("GameSetting UI")]
    public GamePlayManager mGamePlayManager;
    public GameObject GameingUI;

    private GetGoogleOAuth mOuAuth;
    private const string LiveBoadcastURL = "https://youtube.googleapis.com/youtube/v3/liveBroadcasts?part=snippet&broadcastStatus=active";
    private const string superchat = "https://youtube.googleapis.com/youtube/v3/superChatEvents?part=snippet&maxResults=20&";
    private float GetSuperchatUpdate;
    public long LastSCTime;
    public string ChatID;
    void Start()
    {
        mOuAuth = GetComponent<GetGoogleOAuth>();
        GetSuperchatUpdate = 0;
        LastSCTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds()-28800;
        QBQF.Event.EventSystem.Events += GetEvents;
        ChangeStatus(GameStatus.Start);
        StartButton.onClick.AddListener(NextStatus);
    }

    public void GetEvents(object sender, GamePlayEventArgs args)
    {
        if (args.StringParameta.Equals("GetToken"))
        {
            
            if(args.BoolParameta)
            {
                Content.text = mOuAuth.AccessToken;
                NextStep.gameObject.SetActive(true);
            }
            else
            {
                Content.text = "獲取失敗";
                GetAccessButton.gameObject.SetActive(true);
            }
        }
    }

    public void ChangeStatus(GameStatus iStatus)
    {
        StartUI.SetActive(false);
        GetAccessUI.SetActive(false);
        GameingUI.SetActive(false);
        CurStatus = iStatus;
        switch (CurStatus)
        {
            case GameStatus.Start:
                StartUI.SetActive(true);
                break;
            case GameStatus.WaitToken:
                GetAccessUI.SetActive(true);
                Title.text = "Get AccessToken";
                Content.text = string.Empty;
                GetAccessButton.gameObject.SetActive(true);
                NextStep.gameObject.SetActive(false);
                GetAccessButton.onClick.RemoveAllListeners();
                NextStep.onClick.RemoveAllListeners();
                GetAccessButton.onClick.AddListener(GetToken);
                NextStep.onClick.AddListener(NextStatus);
                break;
            case GameStatus.WaitLiveRoom:
                GetAccessUI.SetActive(true);
                Title.text = "Get Live Room ID";
                Content.text = string.Empty;
                GetAccessButton.gameObject.SetActive(true);
                NextStep.gameObject.SetActive(false);
                GetAccessButton.onClick.RemoveAllListeners();
                NextStep.onClick.RemoveAllListeners();
                GetAccessButton.onClick.AddListener(CheckLiveRoomID);
                NextStep.onClick.AddListener(NextStatus);
                break;
            case GameStatus.Gameing:
                GameingUI.SetActive(true);
                break;
        }
    }

    public void GetToken()
    {
        mOuAuth.AuthorizeAsync();
        GetAccessButton.gameObject.SetActive(false);
    }

    public void NextStatus()
    {
        ChangeStatus(CurStatus + 1);
    }

    public void Update()
    {
        if (CurStatus == GameStatus.Gameing)
        {
            GetSuperchatUpdate += Time.deltaTime;
            if (GetSuperchatUpdate >= 10)
            {
                GetSuperchatUpdate = 0;
                StartCoroutine(GetSuperChats());
            }
        }
    }
    #region Check SuperChats

    IEnumerator GetSuperChats()
    {
        string TargetURL = superchat;
        TargetURL += "&access_token=" + mOuAuth.AccessToken;
        Debug.Log(TargetURL);
        UnityWebRequest request = UnityWebRequest.Get(TargetURL);
        yield return request.SendWebRequest();
        if (!request.isNetworkError && !request.isHttpError)
        {
            Debug.LogError("GetDataSucess");
            YT.SuperChat.Root aRoot = JsonUtility.FromJson<YT.SuperChat.Root>(request.downloadHandler.text);
            for (int i = aRoot.items.Length-1; i >= 0; i--)
            {
                long aSCTime = aRoot.items[i].snippet.createdAt.ConverTime();
                SuperChat aSuperChat = new SuperChat(aRoot.items[i].snippet.channelId,
                                                     aRoot.items[i].snippet.supporterDetails.displayName,
                                                     aRoot.items[i].snippet.supporterDetails.profileImageUrl,
                                                     aRoot.items[i].snippet.createdAt.ConverTime(),
                                                     aRoot.items[i].snippet);
                bool IsFound = false;
                if (aSCTime > LastSCTime && !IsFound)
                {
                    mGamePlayManager.superChatPerform.AddSC(aSuperChat);
                    LastSCTime = aSCTime;
                }
            }
        }
        else
        {
            Debug.LogError(request.error);
        }
    }
    #endregion
    #region Check Live Room API
    public void CheckLiveRoomID()
    {
        GetAccessButton.gameObject.SetActive(false);
        StartCoroutine(GetLiveRoomID());
    }

    IEnumerator GetLiveRoomID()
    {
        string TargetURL = LiveBoadcastURL;
        TargetURL += "&access_token=" + mOuAuth.AccessToken;
        Debug.Log(TargetURL);
        UnityWebRequest request = UnityWebRequest.Get(TargetURL);
        yield return request.SendWebRequest();
        if (!request.isNetworkError && !request.isHttpError)
        {
            bool IsFailed = false;
            YT.ChatRoom.Root aRoot = JsonUtility.FromJson<YT.ChatRoom.Root>(request.downloadHandler.text);
            if (aRoot == null || aRoot.items == null || aRoot.items.Length == 0)
            {
                Content.text = "無法獲取聊天室，確認是否開啟直播";
                GetAccessButton.gameObject.SetActive(true);
            }
            else
            {
                ChatID = aRoot.items[0].snippet.liveChatId;
                Content.text = ChatID;
                NextStep.gameObject.SetActive(true);
            }
            QBQF.Event.EventSystem.SendEvent(gameObject, new GamePlayEventArgs("GetLiveRoomID", IsFailed));
        }
        else
        {
            Content.text = "無法獲取聊天室，確認Token是否正確";
            QBQF.Event.EventSystem.SendEvent(gameObject, new GamePlayEventArgs("GetLiveRoomID", false));
            GetAccessButton.gameObject.SetActive(true);
        }
    }
    #endregion
}
