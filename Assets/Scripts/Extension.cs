using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Extension
{
    public static long ConverTime(this string iTime)
    {
        string[] aDate     = iTime.Split('T');
        string[] YMD       = aDate[0].Split('-');
        string[] aTimeZone = aDate[1].Split('+');
        string[] aTime     = aTimeZone[0].Split(':');
        DateTime aDateTime = new DateTime(int.Parse(YMD[0]), int.Parse(YMD[1]), int.Parse(YMD[2]), Mathf.FloorToInt(float.Parse(aTime[0])), Mathf.FloorToInt(float.Parse(aTime[1])), Mathf.FloorToInt(float.Parse(aTime[2])));
        DateTimeOffset dateTimeOffset = new DateTimeOffset(aDateTime);
        return dateTimeOffset.ToUnixTimeSeconds();
    }
}
