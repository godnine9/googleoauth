﻿using System;
using UnityEngine;
public class GamePlayEventArgs : EventArgs
{
    private string stringParameta;
    private bool boolParameta;
    private int intParameta;
    private float floatParameta;
    private Vector3 vector3Paraneta;
    private GameObject go;

    public int IntPrarameta { get { return intParameta; } private set { intParameta = value; } }
    public float FloatParameta { get { return floatParameta; } private set { floatParameta = value; } }
    public Vector3 Vector3Parameta { get { return vector3Paraneta; } private set { vector3Paraneta = value; } }
    public bool BoolParameta { get { return boolParameta; } private set { boolParameta = value; } }

    public string StringParameta { get { return stringParameta; } private set { stringParameta = value; } }
    public GameObject Go { get { return go; } private set { go = value; } }

    public GamePlayEventArgs(bool iBoolParameta = false)
    {
        BoolParameta = iBoolParameta;
    }
    public GamePlayEventArgs(string iMessage, int iIntParameta, float iFloatParameta, Vector3 iVector3Paraneta, GameObject iGo = null)
    {
        StringParameta = iMessage;
        IntPrarameta = iIntParameta;
        FloatParameta = iFloatParameta;
        Vector3Parameta = iVector3Paraneta;
        Go = iGo;
    }

    public GamePlayEventArgs(string iMessage, bool iBoolParameta = false)
    {
        StringParameta = iMessage;
        boolParameta = iBoolParameta;
    }

    public GamePlayEventArgs(int iIntParameta, bool iBoolParameta = false)
    {
        IntPrarameta = iIntParameta;
        BoolParameta = iBoolParameta;
    }

    public GamePlayEventArgs(int iIntParameta, float iFloatParameta)
    {
        IntPrarameta = iIntParameta;
        floatParameta = iFloatParameta;
    }

    public GamePlayEventArgs(float iFloatParameta)
    {
        FloatParameta = iFloatParameta;
    }

    public GamePlayEventArgs(Vector3 iVector3Paraneta, bool iBoolParameta = false)
    {
        Vector3Parameta = iVector3Paraneta;
        BoolParameta = iBoolParameta;
    }
}
