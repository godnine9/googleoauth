﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QBQF.Event
{
    public static class EventSystem
    {
        public static event System.EventHandler<GamePlayEventArgs> Events;

		public static void SendEvent(GameObject Sender, GamePlayEventArgs iArg)
		{
			if (Sender is null)
			{
				throw new ArgumentNullException(nameof(Sender));
			}

			GamePlayEventArgs aEvent = iArg;
			Events?.Invoke(Sender, aEvent);
		}
	}
}
