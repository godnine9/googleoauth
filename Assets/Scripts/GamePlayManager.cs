﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    public static GamePlayManager Instance;
    public Timmer timmer;
    public SuperChatPerform superChatPerform;
    public Wheel NormalWheel;
    public Wheel HeightLevelWheel;
    public Strength strength;

    public void Start()
    {
        Init();
    }

    public void Init()
    {

        if(PlayerPrefs.HasKey("Strength"))
        {
            strength.Init(PlayerPrefs.GetInt("Strength"));
        }
        else
        {
            strength.Init(12);
        }
        if (PlayerPrefs.HasKey("LeftTime"))
        {
            timmer.Init(PlayerPrefs.GetFloat("LeftTime"));
        }
        else
        {
            timmer.Init(1200);
        }
        superChatPerform.Init(this);
        NormalWheel.Init(this);
        HeightLevelWheel.Init(this);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            SuperChatData aSuperChatData = Resources.Load<SuperChatData>("FackSuperChatData");
            SuperChat aSuperChat = aSuperChatData.SuperChats[Random.Range(0, aSuperChatData.SuperChats.Length)];
            superChatPerform.AddSC(aSuperChat);
        }
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (NormalWheel.WheelRoot.localScale.x == 0)
            {
                NormalWheel.Show();
            }
            else if (NormalWheel.WheelRoot.localScale.x == 1)
            {
                NormalWheel.Hide();
            }
            if(HeightLevelWheel.WheelRoot.localScale.x >0)
            {
                HeightLevelWheel.Hide();
            }
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            if (HeightLevelWheel.WheelRoot.localScale.x == 0)
            {
                HeightLevelWheel.Show();
            }
            else if (HeightLevelWheel.WheelRoot.localScale.x == 1)
            {
                HeightLevelWheel.Hide();
            }
            if (NormalWheel.WheelRoot.localScale.x > 0)
            {
                NormalWheel.Hide();
            }
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            if (NormalWheel.WheelRoot.localScale.x == 0)
            {
                NormalWheel.RotateWheel();
            }
            else if (NormalWheel.WheelRoot.localScale.x == 1)
            {
                NormalWheel.Hide();
            }
            if (HeightLevelWheel.WheelRoot.localScale.x > 0)
            {
                HeightLevelWheel.Hide();
            }
        }
        if (Input.GetKeyDown(KeyCode.F7))
        {
            if (HeightLevelWheel.WheelRoot.localScale.x == 0)
            {
                HeightLevelWheel.RotateWheel();
            }
            else if (HeightLevelWheel.WheelRoot.localScale.x == 1)
            {
                HeightLevelWheel.Hide();
            }
            if (NormalWheel.WheelRoot.localScale.x > 0)
            {
                NormalWheel.Hide();
            }
        }
    }
}
