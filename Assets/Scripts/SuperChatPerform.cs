﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using YT.SuperChat;
using SpeechLib;
//引入庫
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class SuperChatPerform : MonoBehaviour
{
    public Color[] TypeColor;
    [Header("SuperChat")]
    public Image SuperChatBG;
    public Image ActorImg;
    public Text  Money;
    public Text  Name;
    public Text  Content;

    public List<SuperChat> PerformSC = new List<SuperChat>();
    private bool IsPreforming = false;
    private SpVoice voice;
    private GamePlayManager mGamePlayManager;
    public void Init(GamePlayManager iGamePlayManager)
    {
        mGamePlayManager = iGamePlayManager;
        voice = new SpVoice();
        voice.Voice = voice.GetVoices(string.Empty, string.Empty).Item(0);
        voice.Rate = 0;
        voice.Volume = 100;
        SuperChatBG.rectTransform.localScale = Vector3.zero;
    }

    public void Update()
    {
        if(!IsPreforming && PerformSC.Count>0)
        {
            StartCoroutine(SCPreform());
        }
    }

    public void AddSC(SuperChat iSC)
    {
        PerformSC.Add(iSC);
    }

    IEnumerator SCPreform()
    {
        IsPreforming = true;
        SuperChat aSuperChat = PerformSC[0];
        PerformSC.RemoveAt(0);
        WWW www = new WWW(aSuperChat.profileImageUrl);
        yield return www;
        ActorImg.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        Name.text = aSuperChat.displayName;
        Money.text = aSuperChat.Snippet.displayString;
        Content.text = aSuperChat.Snippet.commentText;
        SuperChatBG.color = TypeColor[aSuperChat.Snippet.messageType - 1];
        SuperChatBG.rectTransform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.1f).OnComplete(() =>
            SuperChatBG.rectTransform.DOScale(new Vector3(0.8f, 0.8f, 0.8f), 0.05f).OnComplete(() => 
                SuperChatBG.rectTransform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.05f)));
        string SpeakContent = aSuperChat.displayName + "說" + aSuperChat.Snippet.commentText;
        voice.Speak(SpeakContent, SpeechVoiceSpeakFlags.SVSFlagsAsync);
        switch (aSuperChat.Snippet.messageType)
        {
            case 3:
                mGamePlayManager.timmer.AddTime(120);
                break;
            case 5:
                mGamePlayManager.NormalWheel.RotateWheel();
                break;
            case 6:
                mGamePlayManager.strength.AddStrength(2);
                break;
            case 7:
                mGamePlayManager.HeightLevelWheel.RotateWheel();
                break;
        }
        yield return new WaitForSeconds(SpeakContent.Length*0.25f + 1.5f);
        SuperChatBG.rectTransform.DOScale(new Vector3(0,0,0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        voice.Pause();
        voice.Resume();
        yield return new WaitForSeconds(8.0f);
        IsPreforming = false;
    }

    #region
    //以下預設都是私有的成員
    Socket serverSocket; //伺服器端socket
    Socket clientSocket; //客戶端socket
    IPEndPoint ipEnd; //偵聽埠
    string recvStr; //接收的字串
    string sendStr; //傳送的字串
    byte[] recvData = new byte[1024]; //接收的資料，必須為位元組
    byte[] sendData = new byte[1024]; //傳送的資料，必須為位元組
    int recvLen; //接收的資料長度
    Thread connectThread; //連線執行緒

    void Start()
    {
        InitSocket(); //在這裡初始化server
    }

    //初始化
    void InitSocket()
    {
        //定義偵聽埠,偵聽任何IP
        ipEnd = new IPEndPoint(IPAddress.Any, 5566);
        //定義套接字型別,在主執行緒中定義
        serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        //連線
        serverSocket.Bind(ipEnd);
        //開始偵聽,最大10個連線
        serverSocket.Listen(10);



        //開啟一個執行緒連線，必須的，否則主執行緒卡死
        connectThread = new Thread(new ThreadStart(SocketReceive));
        connectThread.Start();
    }

    //連線
    void SocketConnet()
    {
        if (clientSocket != null)
            clientSocket.Close();
        //控制檯輸出偵聽狀態
        print("Waiting for a client");
        //一旦接受連線，建立一個客戶端
        clientSocket = serverSocket.Accept();
        //獲取客戶端的IP和埠
        IPEndPoint ipEndClient = (IPEndPoint)clientSocket.RemoteEndPoint;
        //輸出客戶端的IP和埠
        print("Connect with " + ipEndClient.Address.ToString() + ":" + ipEndClient.Port.ToString());
        //連線成功則傳送資料
        sendStr = "Welcome to my server";
        SocketSend(sendStr);
    }

    void SocketSend(string sendStr)
    {
        //清空傳送快取
        sendData = new byte[1024];
        //資料型別轉換
        Encoding Utf8 = Encoding.UTF8;
        sendData = Utf8.GetBytes(sendStr);
        //傳送
        clientSocket.Send(sendData, sendData.Length, SocketFlags.None);
    }

    //伺服器接收
    void SocketReceive()
    {
        //連線
        SocketConnet();
        //進入接收迴圈
        while (true)
        {
            //對data清零
            recvData = new byte[1024];
            //獲取收到的資料的長度
            recvLen = clientSocket.Receive(recvData);
            //如果收到的資料長度為0，則重連並進入下一個迴圈
            if (recvLen == 0)
            {
                SocketConnet();
                continue;
            }
            //輸出接收到的資料
            Encoding Utf8 = Encoding.UTF8;
            recvStr = Utf8.GetString(recvData, 0, recvLen);
            print(recvStr);

            string[] Meesages = recvStr.Split('-');
            if(Meesages.Length ==3)
            {
                YT.SuperChat.Snippet aSnippet = new Snippet();
                aSnippet.messageType   = int.Parse(Meesages[0]);
                aSnippet.displayString = Meesages[1];
                aSnippet.commentText = Meesages[2];
                SuperChat aNesSC = new SuperChat("UCYI1zc9CKyMlZHzhwf_W92A", "UMiLive Coding Monkey Ch.", "https://yt3.ggpht.com/mm4keifPMJCEaf8dIld5cf_HjpgDHQ4bAV95toSWUA8XcTtPE4f86_siVIWiwP2EeT2D7JXR=s88-c-k-c0x00ffffff-no-rj-mo", 0, aSnippet);
                AddSC(aNesSC);
            }
            
            //將接收到的資料經過處理再發送出去
            //sendStr = "From Server: " + recvStr;
            //SocketSend(sendStr);
        }
    }

    //連線關閉
    void SocketQuit()
    {
        //先關閉客戶端
        if (clientSocket != null)
            clientSocket.Close();
        //再關閉執行緒
        if (connectThread != null)
        {
            connectThread.Interrupt();
            connectThread.Abort();
        }
        //最後關閉伺服器
        serverSocket.Close();
        print("diconnect");
    }

    void OnApplicationQuit()
    {
        SocketQuit();
    }
    #endregion
}
