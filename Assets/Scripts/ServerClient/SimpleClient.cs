﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine.UI;
using System;
public class SimpleClient : MonoBehaviour
{
    public InputField IPInput;
    public Dropdown MessageType;
    public InputField MessageInput;
    public Button InitSoketButton;
    public Button SendButton;

    string editString = "hello wolrd"; //編輯框文字

    Socket serverSocket; //伺服器端socket
    IPAddress ip; //主機ip
    IPEndPoint ipEnd;
    string recvStr; //接收的字串
    string sendStr; //傳送的字串
    byte[] recvData = new byte[1024]; //接收的資料，必須為位元組
    byte[] sendData = new byte[1024]; //傳送的資料，必須為位元組
    int recvLen; //接收的資料長度
    Thread connectThread; //連線執行緒

    // Use this for initialization
    void Start()
    {
        IPInput.text = "127.0.0.1";
        MessageInput.text = editString;
        InitSoketButton.onClick.AddListener(() => InitSocket());
        SendButton.onClick.AddListener(() => {
            string aMessageType = (MessageType.value+1).ToString();
            string aMessageMoney = "0";
            switch (MessageType.value)
            {
                case 0:
                    aMessageMoney = "15";
                    break;
                case 1:
                    aMessageMoney = "30";
                    break;
                case 2:
                    aMessageMoney = "75";
                    break;
                case 3:
                    aMessageMoney = "150";
                    break;
                case 4:
                    aMessageMoney = "300";
                    break;
                case 5:
                    aMessageMoney = "750";
                    break;
                case 6:
                    aMessageMoney = "1500";
                    break;
            }
            SocketSend($"{aMessageType}-{aMessageMoney}-{MessageInput.text}");
        });
    }

    //初始化
    void InitSocket()
    {
        IPInput.gameObject.SetActive(false);
        InitSoketButton.gameObject.SetActive(false);
        //定義伺服器的IP和埠，埠與伺服器對應
        ip = IPAddress.Parse(IPInput.text); //可以是區域網或網際網路ip，此處是本機
        ipEnd = new IPEndPoint(ip, 5566);

        //開啟一個執行緒連線，必須的，否則主執行緒卡死
        connectThread = new Thread(new ThreadStart(SocketReceive));
        connectThread.Start();
    }

    void SocketConnet()
    {
        if (serverSocket != null)
            serverSocket.Close();
        //定義套接字型別,必須在子執行緒中定義
        serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        print("ready to connect");
        //連線
        serverSocket.Connect(ipEnd);

        //輸出初次連線收到的字串
        recvLen = serverSocket.Receive(recvData);
        Encoding Utf8 = Encoding.UTF8;
        recvStr = Utf8.GetString(recvData, 0, recvLen);
        print(recvStr);
    }

    void SocketSend(string sendStr)
    {
        //清空傳送快取
        sendData = new byte[1024];
        //資料型別轉換
        Encoding Utf8 = Encoding.UTF8;
        sendData = Utf8.GetBytes(sendStr);
        //傳送
        serverSocket.Send(sendData, sendData.Length, SocketFlags.None);
    }

    void SocketReceive()
    {
        SocketConnet();
        //不斷接收伺服器發來的資料
        while (true)
        {
            recvData = new byte[1024];
            recvLen = serverSocket.Receive(recvData);
            if (recvLen == 0)
            {
                SocketConnet();
                continue;
            }
            recvStr = Encoding.ASCII.GetString(recvData, 0, recvLen);
            print(recvStr);
        }
    }

    void SocketQuit()
    {
        //關閉執行緒
        if (connectThread != null)
        {
            connectThread.Interrupt();
            connectThread.Abort();
        }
        //最後關閉伺服器
        if (serverSocket != null)
            serverSocket.Close();
        print("diconnect");
    }


    //程式退出則關閉連線
    void OnApplicationQuit()
    {
        SocketQuit();
    }
}
