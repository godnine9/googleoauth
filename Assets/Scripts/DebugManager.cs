﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    struct Log
    {
        public string message;
        public string stackTrace;
        public LogType type;
    }
    public enum LogLevel
    {
        Debug,
        Warning,
        Error
    }
    public LogLevel m_LogLevel = LogLevel.Debug;
    private const int MaxLogCount = 200;
    private Vector2 scrollPosition;
    public bool IsShow;
    private bool collapse = true;
    private const int margin = 20;
    private Rect windowRect = new Rect(margin, margin, Screen.width - (margin * 2), Screen.height - (margin * 2));
    private readonly Rect titleBarRect = new Rect(0, 0, 10000, 20);
    private readonly GUIContent clearLabel = new GUIContent("Clear Log");
    private readonly GUIContent collapseLabel = new GUIContent("Collapse");
    private readonly GUIContent WarningLog = new GUIContent("Warning Log");
    private readonly GUIContent ErrorLog = new GUIContent("Error Log");
    private readonly GUIContent DebugLogContent = new GUIContent("Debug Log");
    private readonly GUIStyle style = new GUIStyle(), button_style = new GUIStyle();
    private readonly List<Log> logs = new List<Log>();
    static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>()
    {
        { LogType.Assert, Color.white },
        { LogType.Error, Color.red },
        { LogType.Exception, Color.red },
        { LogType.Log, Color.white },
        { LogType.Warning, Color.yellow },
    };
    
    public void Start()
    {
        Application.logMessageReceived += OnDebugLog;
        button_style.fontSize = 20;
        IsShow = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && Input.GetKeyDown(KeyCode.Mouse1))
        {
            Debug.LogError("QAQ");
            IsShow = !IsShow;
        }
    }

    void OnGUI()
    {
        if (!IsShow)
        {
            return;
        }        
        windowRect = GUILayout.Window(122125, windowRect, ConsoleWindow, "Console");
    }

    private void ConsoleWindow(int windowID)
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        string Message = string.Empty;
        for (int i = logs.Count - 1; i >= 0; i--)
        {
            var log = logs[i];
            if (collapse)
            {
                if (i > 0 && log.message == logs[i - 1].message)
                {
                    continue;
                }
            }
            if (m_LogLevel == LogLevel.Warning)
            {
                if (log.type == LogType.Log) continue;
            }
            else if (m_LogLevel == LogLevel.Error)
            {
                if (log.type == LogType.Log || log.type == LogType.Warning) continue;
            }
            style.fontSize = 25;
            style.normal.textColor = logTypeColors[log.type];
            //GUI.contentColor = logTypeColors[log.type];
            Message += log.message + "\n";
            GUILayout.Label(log.message, style);
        }
        GUILayout.EndScrollView();

        GUI.contentColor = Color.white;

        GUILayout.BeginHorizontal();


        var recover_size = GUI.skin.button.fontSize;
        GUI.skin.button.fontSize = 24;
        const int button_width = 200, button_height = 80;


        if (GUILayout.Button(clearLabel, GUILayout.Width(button_width), GUILayout.Height(button_height)))
        {
            logs.Clear();
        }
        if (GUILayout.Button(ErrorLog, GUILayout.Width(button_width), GUILayout.Height(button_height)))
        {
            m_LogLevel = LogLevel.Error;
        }
        if (GUILayout.Button(WarningLog, GUILayout.Width(button_width), GUILayout.Height(button_height)))
        {
            m_LogLevel = LogLevel.Warning;
        }
        if (GUILayout.Button(DebugLogContent, GUILayout.Width(button_width), GUILayout.Height(button_height)))
        {
            m_LogLevel = LogLevel.Debug;
        }

        collapse = GUILayout.Toggle(collapse, collapseLabel, GUILayout.Width(button_width), GUILayout.Height(button_height));

        GUILayout.EndHorizontal();

        // Allow the window to be dragged by its title bar.
        GUI.DragWindow(titleBarRect);
        GUI.skin.button.fontSize = recover_size;
    }
    public void OnDebugLog(string message, string stackTrace = "", LogType type = LogType.Log)
    {
        if (logs.Count >= MaxLogCount)
        {
            logs.RemoveAt(0);
        }
        logs.Add(new Log()
        {
            message = message,
            stackTrace = stackTrace,
            type = type,
        });
    }
}
