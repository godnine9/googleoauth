﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Timmer : MonoBehaviour
{
    public Button StartBtn;
    public TMP_Text BtnText;
    public Button ResetBtn;

    public Button Add10Btn;
    public Button Add5Btn;
    public Button Add1Btn;
    public Button Reduce10Btn;
    public Button Reduce5Btn;
    public Button Reduce1Btn;

    public TMP_InputField HourInput;
    public TMP_InputField MinInput;
    public TMP_InputField SecInput;

    public float CurTime;
    private float TimerUpdate;
    private bool IsCounterStart;
    private bool IsPasue;
    public void Init(float iTime)
    {
        HourInput.onSelect.AddListener(StartEditTimer);
        MinInput.onSelect.AddListener(StartEditTimer);
        SecInput.onSelect.AddListener(StartEditTimer);
        HourInput.onEndEdit.AddListener(EndEditTimer);
        MinInput.onEndEdit.AddListener(EndEditTimer);
        SecInput.onEndEdit.AddListener(EndEditTimer);
        StartBtn.   onClick.AddListener(StartButton);
        ResetBtn.   onClick.AddListener(ResetButton);
        Add10Btn.   onClick.AddListener(delegate { AddTime( 600); });
        Add5Btn.    onClick.AddListener(delegate { AddTime( 300); });
        Add1Btn.    onClick.AddListener(delegate { AddTime( 60); });
        Reduce10Btn.onClick.AddListener(delegate { AddTime(-600); });
        Reduce5Btn. onClick.AddListener(delegate { AddTime(-300); });
        Reduce1Btn. onClick.AddListener(delegate { AddTime(-60); });
        IsCounterStart = false;
        IsPasue = false;
        CurTime = iTime;
        TimeUpdate();
    }

    // Update is called once per frame
    void Update()
    {
        if(IsCounterStart && !IsPasue)
        {
            TimerUpdate += Time.deltaTime;
            if (TimerUpdate >= 1)
            {
                TimerUpdate = 0;
                CurTime--;
                TimeUpdate();
                if (CurTime <= 0)
                {
                    ResetButton();
                }
            }
        }
    }

    private void StartButton()
    {
        IsCounterStart = !IsCounterStart;
        BtnText.text = IsCounterStart ? "Pause" : "Start";
    }
    private void ResetButton()
    {
        CurTime = 0;
        TimeUpdate();
        IsCounterStart = false;
    }
    public void AddTime(float iValue)
    {
        CurTime += iValue;
       if(CurTime<=0)
        {
            CurTime = 0;
        }
        TimeUpdate();
    }
    public void MultiplyTime(float iValue)
    {
        CurTime *= iValue;
        TimeUpdate();
    }

    private void StartEditTimer(string iValue)
    {
        IsPasue = true;
    }
    private void EndEditTimer(string iValue)
    {
        IsPasue = false;
        CaculatTime();
    }

    private void CaculatTime()
    {
        CurTime = int.Parse(HourInput.text) * 3600+ int.Parse(MinInput.text) * 60 + int.Parse(SecInput.text);
    }
    private void TimeUpdate()
    {
        HourInput.text = Mathf.FloorToInt(CurTime/3600).ToString();
        if(CurTime % 3600 == 0)
        {
            MinInput.text = "0";
        }
        else
        {
            MinInput.text = Mathf.FloorToInt((CurTime % 3600) / 60).ToString();
        }
        if (CurTime % 60 == 0)
        {
            SecInput.text = "0";
        }
        else
        {
            SecInput.text = Mathf.FloorToInt((CurTime % 60)).ToString();
        }
    }
}
