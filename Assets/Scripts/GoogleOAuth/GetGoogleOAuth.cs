﻿using GoogleOAuth;
using UnityEngine;
using UnityEngine.UI;
public class GetGoogleOAuth : MonoBehaviour
{
    [SerializeField] private string _clientId;
    [SerializeField] private string _clientSecret;
    public string AccessToken;
    private AuthorizationCodeProvider.Result _authorizationResult;
    private AccessTokenProvider.Result _accessTokenResult;
    private bool IsGetAccessToken = false;
    private float TimeUpdate;
    public async void AuthorizeAsync()
    {
        var provider = new AuthorizationCodeProvider(_clientId);
        var handle = await provider.ProvideAsync();
        if (!handle.IsFailed)
        {
            _authorizationResult = handle.Result;
        }
        provider.Dispose();
        Debug.Log($"Authorization Code: {_authorizationResult.AuthorizationCode}");
        GetAccessTokenAsync();
    }

    public async void GetAccessTokenAsync()
    {
        Debug.Log("DoGetAccessTokenAsync");
        var provider = new AccessTokenProvider(_clientId, _clientSecret);
        var handle = await provider.ProvideAsync(_authorizationResult.AuthorizationCode, _authorizationResult.CodeVerifier, _authorizationResult.RedirectUri);
        if (!handle.IsFailed)
        {
            _accessTokenResult = handle.Result;
            IsGetAccessToken = true;
            TimeUpdate = 0;
        }
        AccessToken = _accessTokenResult.AccessToken;
        QBQF.Event.EventSystem.SendEvent(gameObject, new GamePlayEventArgs("GetToken", !handle.IsFailed));
        Debug.LogError($"Access Token: {_accessTokenResult.AccessToken}");
    }

    public async void RefreshAccessTokenAsync()
    {
        var provider = new RefreshedAccessTokenProvider(_clientId, _clientSecret);
        var handle = await provider.ProvideAsync(_accessTokenResult.RefreshToken);
        if (!handle.IsFailed)
        {
            AccessToken = handle.Result.AccessToken;
        }
        Debug.LogError($"Access Token: {handle.Result.AccessToken}");
    }

    public void Update()
    {
        if(IsGetAccessToken)
        {
            TimeUpdate += Time.deltaTime;
            if(TimeUpdate>=3600)
            {
                TimeUpdate=0;
                RefreshAccessTokenAsync();
            }
        }
    }
}